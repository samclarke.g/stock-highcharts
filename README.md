# Non-Technical Overviw

This application consists of 2 core components:

1. Flask backend web app. This backend logic is contained in `app.py` and is responsible for static authentication and serving the `index.html` whicch contains the main markkup and loads the frontend business logic. The backend is also responsible for saving and loading the session data to and from the file system as `.json` files using the filename pattern `<username>-data.json`. The backend has a standard virtualenvironemnt implementation and uses a simple `.wsgi` file to interface with the web server request/ rsponse cycle. The app can typically be deployed with a *gunicorn* web server.
    
2. The FrontEnd is implemented using `Vue.js` and the `Stock Highcharts` library (with a Vue wrapper). One main App component mounts to the `index.html` file and loads a single Stockcharts component which houses the charting and interactivity logic. The build process is handled by `webpack` and outputs a single `dist/build.js` source file. Typically the webserver is configured to server this file as a static asset. All frontend source files are contained in the `src` directory, and can be served alone using `npm run dev`.
    

# Python Flask server

## Backend Setup

``` bash
# create virtualenv and install dependencies
mkvirtualenv stock-highcharts
pip install -r requirements.txt

# run dev server
python app.py
```


# vue-highcharts

> A Vue.js project

## Frontend Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).
=======
# stock-highcharts


## Running in Production using Gunicorn

``` bash
# use a process manager such as Upstart or SystemD 
/path/to/virtualenv/bin/gunicorn --access-logfile - --workers 3 --bind unix:stock-highcharts.sock wsgi:app
```
