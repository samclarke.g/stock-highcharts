from flask import Flask, session, redirect, url_for, escape, request, render_template
import json

app = Flask(__name__, template_folder='.', static_folder = 'dist')

user_whitelist = ['sam', 'jon']

# Set the secret key to some random bytes. Keep this really secret!
app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'

@app.route('/')
def index():
    if 'username' in session:
        # return 'Logged in as %s' % escape(session['username'])
        return render_template('./index.html', username=session['username'] or None)
    return redirect(url_for('login'))

@app.route('/login', methods=['GET', 'POST'])
def login():
    error_msg = None
    if request.method == 'POST' and request.form['username'] in user_whitelist:
        session['username'] = request.form['username']
        return redirect(url_for('index'))
    elif request.method == 'POST' and request.form['username'] not in user_whitelist:
        error_msg = 'username not recognized'
    return render_template('./login.html', error_msg=error_msg)

@app.route('/logout')
def logout():
    # remove the username from the session if it's there
    session.pop('username', None)
    return redirect(url_for('login'))


#  API Endpoints
@app.route('/save', methods=['POST'])
def save():
    data = None
    if request.method == 'POST' and request.data:
        with open('dist/{}-data.json'.format(session['username']), 'w') as f:
            json.dump(request.data, f)
    return request.data
    


if __name__ == '__main__':
    app.run(debug=True)