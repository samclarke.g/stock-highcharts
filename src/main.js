import Vue from 'vue'
import App from './App.vue'
import HighchartsVue from 'highcharts-vue'
import Highcharts from 'highcharts'
import stockInit from 'highcharts/modules/stock'
import Indicators from 'highcharts/indicators/indicators'


stockInit(Highcharts)
Indicators(Highcharts)

Vue.use(HighchartsVue)

new Vue({
  el: '#app',
  render: h => h(App)
})